use std::env;
use std::fs::File;
use std::io::Read;
use chippit::emulator::Emulator;
use flatland::eventhandlers::{Event, Key};

fn print_something(_e: &mut Emulator){
    println!("TEST COMMAND");
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = match &args.len() {
        1 => "./test.ch8",
        _ => &args[1]
    };
    let mut file = File::open(filename).unwrap();
    let mut buf: Vec<u8> = Vec::with_capacity(file.metadata().unwrap().len() as usize);
    file.read_to_end(&mut buf).unwrap();
    let mut emu = Emulator::new(
        None,
        vec!(
            (Event::KeyPress(Key::T), print_something),
        ));
    emu.update_program(buf.as_slice());
    emu.run();
}


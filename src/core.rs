use rand;
use rand::Rng;
use crate::opcodes::Opcode;
use std::time;

const FONTSET:[u8; 80] = [
    0xF0, 0x90, 0x90, 0x90, 0xF0, // 0
    0x40, 0x40, 0x40, 0x40, 0x40, // 1
    0xF0, 0x10, 0xF0, 0x80, 0xF0, // 2
    0xF0, 0x10, 0xF0, 0x10, 0xF0, // 3
    0x90, 0x90, 0xF0, 0x10, 0x10, // 4
    0xF0, 0x80, 0xF0, 0x10, 0xF0, // 5
    0xF0, 0x80, 0xF0, 0x90, 0xF0, // 6
    0xF0, 0x10, 0x20, 0x40, 0x40, // 7
    0xF0, 0x90, 0xF0, 0x90, 0xF0, // 8
    0xF0, 0x90, 0xF0, 0x10, 0xF0, // 9
    0xF0, 0x90, 0xF0, 0x90, 0x90, // A
    0xE0, 0x90, 0xE0, 0x90, 0xE0, // B
    0xF0, 0x80, 0x80, 0x80, 0xF0, // C
    0xE0, 0x90, 0x90, 0x90, 0xE0, // D
    0xF0, 0x80, 0xF0, 0x80, 0xF0, // E
    0xF0, 0x80, 0xF0, 0x80, 0x80, // F
];

fn setPixel(mut canvas: &mut [u8; 8192], x:usize, y:usize, on: bool) -> bool {
    //Canvas is a BGRA material... 64x32
    let index = (y * 64*4) + x*4;
    let output = canvas[index] > 0; // Determine if we're flipping a bit
    let val: u8 = match (on && !output)  {
        true => 255,
        false => 0
    };
    canvas[index] = val;
    canvas[index+1] = val;
    canvas[index+2] = val;
    canvas[index+3] = val;
    return output
}

pub struct Chippit{
    index_reg: usize,
    mem: [u8; 4096],
    program_counter: usize,
    stack: [usize; 16],
    stack_pointer: usize,
    reg: [u8; 16],
    canvas: [u8; 8192],
    input: [u8; 16],
    delay_t: u8,
    delay_instant: time::Instant,

    delay_s: u8,
    draw_flag: bool,
    inc_size: usize,
    rng: rand::rngs::ThreadRng,
}

impl Chippit{
    pub fn load(bin: &[u8]) -> Chippit{
        let mut mem = [0; 4096];
        
        // Fill up the fontset...
        for (i, &val) in FONTSET.iter().enumerate(){
            mem[i] = val;
        };

        // Then fill up the mem...
        for (i, &val) in bin.iter().enumerate(){
            mem[i+512] = val;
        };

        Chippit{
            program_counter: 0x200,
            index_reg: 0, stack_pointer: 0,
            reg: [0; 16], input: [0; 16], stack: [0; 16],
            canvas: [0; 8192],
            delay_t: 0, delay_instant: time::Instant::now(), delay_s: 0, draw_flag: false,
            inc_size: 2, rng: rand::thread_rng(), mem,
        }

    }

    pub fn cycle(&mut self, keys: &[bool; 16]) -> Option<&[u8]>{
        for (i, x) in keys.iter().enumerate(){
            self.input[i] = match x{
                true => 1,
                false => 0
            }
        }
        self.doop();
        self.program_counter += self.inc_size;
        if self.draw_flag{
            return Some(&self.canvas)
        }
        return None
    }

    fn doop(&mut self){
        // Default byte increment size...
        self.inc_size = 2;
        self.draw_flag = false;

        if self.program_counter >= self.mem.len(){
            return ()
        }
        let opcode = (self.mem[self.program_counter], self.mem[self.program_counter + 1]);
        let op = Opcode::from(opcode);
         //println!(
         //    "@{}: {:?}({:b} {:b}) => {:?}",
         //    self.program_counter,
         //    opcode,
         //    self.mem[self.program_counter],
         //    self.mem[self.program_counter + 1],
         //    op
         //);
         //println!(
         //    "v0:{} v1:{} v2:{} v3:{} v4:{} v5:{} v6:{} v7:{}",
         //    self.reg[0], self.reg[1], self.reg[2], self.reg[3], self.reg[4],
         //    self.reg[5], self.reg[6], self.reg[7]
         //   );
         //println!(
         //    "v8:{} v9:{} vA:{} vB:{} vC:{} vD:{} vE:{} vF:{}",
         //    self.reg[8], self.reg[9], self.reg[10], self.reg[11], self.reg[12],
         //    self.reg[13], self.reg[14], self.reg[15]
         //   );
        match op {
            Opcode::ClearScreen => {
                self.canvas = [0;8192];
                self.draw_flag = true;
            },
            Opcode::Jump(n) => {
                self.program_counter = n as usize;
                self.inc_size = 0;
            },
            Opcode::Call(n) => {
                self.stack[self.stack_pointer] = self.program_counter + 2;
                self.stack_pointer += 1;
                self.program_counter = n as usize;
                self.inc_size = 0;
            },
            Opcode::Return => {
                self.stack_pointer -= 1;
                self.program_counter = self.stack[self.stack_pointer];
                self.inc_size = 0;
            },
            Opcode::SkipXisB(x, b) => {
                if self.reg[x] == b{
                    self.inc_size = 4;
                }
            },
            Opcode::SkipXnotB(x, b) => {
                if self.reg[x] != b{
                    self.inc_size = 4;
                }
            },
            Opcode::SkipXisY(x, y) => {
                if self.reg[x] == self.reg[y]{
                    self.inc_size = 4;
                }
            },
            Opcode::SetXfromB(x, b) => {
                self.reg[x] = b;
            },
            Opcode::AddXB(x, b) => {
                let (val, _) = self.reg[x].overflowing_add(b);
                self.reg[x] = val;
            },
            Opcode::SetXfromY(x, y) => {
                self.reg[x] = self.reg[y];
            },
            Opcode::ORXY(x, y) => {
                self.reg[x] |= self.reg[y];
            },
            Opcode::ANDXY(x, y) => {
                self.reg[x] &= self.reg[y];
            },
            Opcode::XORXY(x, y) => {
                self.reg[x] ^= self.reg[y];
            },
            Opcode::AddXY(x, y) => {
                let (val, over) = self.reg[x].overflowing_add(self.reg[y]);
                self.reg[0xF] = over as u8;
                self.reg[x] = val;
            },
            Opcode::SubXY(x, y) => {
                let (val, overflow) = self.reg[x].overflowing_sub(self.reg[y]);
                self.reg[0xF] = match overflow {
                    false => 1,
                    true => 0
                };
                self.reg[x] = val;
            },
            Opcode::SubnXY(x, y) => {
                let (val, overflow) = self.reg[y].overflowing_sub(self.reg[x]);
                self.reg[0xF] = match overflow {
                    false => 1,
                    true => 0
                };
                self.reg[x] = val;
            },
            Opcode::RshiftXY(x, _) => {
                self.reg[15] = self.reg[x] & 0x01;
                self.reg[x] = self.reg[x] >> 1;
            },
            Opcode::LshiftXY(x, _) => {
                self.reg[15] = match self.reg[x] & 0x80{
                    0 => 0,
                    _ => 1,
                };
                self.reg[x] = (self.reg[x] & 0x7F) << 1;
            },
            Opcode::SkipXnotY(x, y) => {
                if self.reg[x] != self.reg[y]{
                    self.inc_size = 4;
                }
            },
            Opcode::SetIfromN(n) => {
                self.index_reg = n
            },
            Opcode::JumpPlusV0(n) => {
               self.program_counter = n + self.reg[0] as usize;
               self.inc_size = 0;
            },
            Opcode::RandXB(x, b) => {
                let r: u8 = self.rng.gen();
                self.reg[x] = r & b;
            },
            Opcode::SetXfromDT(x) => {
                // Determine how many seconds has passed since DT was set..
                let dur = time::Instant::now().duration_since(self.delay_instant);
                // Timer moves at 60hz, how many UNITS have we moved?
                let moved_units = (
                    0.06 *
                    ((dur.as_secs() * 1000) + dur.subsec_millis() as u64) as f64
                ) as u64;
                match moved_units{
                    n if n > 255 => self.reg[x] = 0,
                    n if n as u8 > self.delay_t => self.reg[x] = 0, 
                    n if n as u8 <= self.delay_t => self.reg[x] = self.delay_t - n as u8,
                    _ => self.reg[x] = 0
                }
            },
            Opcode::SetDTfromX(x) => {
                // Set the initial time...
                self.delay_t = self.reg[x];
                self.delay_instant = time::Instant::now();
            },
            Opcode::SetSTfromX(x) => {
                self.delay_s = self.reg[x];
            },
            Opcode::IncIfromX(x) => {
                self.index_reg += self.reg[x] as usize;
                self.reg[0xF] = match self.index_reg > 0x0FFF{
                    true => 1,
                    false => 0
                };
            },
            Opcode::Digit(x) => {
                self.index_reg = ((self.reg[x] & 0x0F) * 5) as usize
            },
            Opcode::WriteMem(x) => {
                for i in 0..=x{
                    self.mem[self.index_reg + i] = self.reg[i];
                }
                self.index_reg += x+1;
            },
            Opcode::ReadMem(x) => {
                for i in 0..=x{
                    self.reg[i] = self.mem[self.index_reg + i];
                }
                self.index_reg += x+1;
            },
            Opcode::WaitKey(x) => { // TEMP
                self.inc_size = 0;
                for (i, k) in self.input.iter().enumerate(){
                    if *k != 0{
                        self.inc_size = 2;
                        self.reg[x] = i as u8;
                        break;
                    }
                }
            },
            Opcode::SkipKeyPressed(x) => {
                if self.input[self.reg[x] as usize] != 0{
                    self.inc_size = 4;
                }
            },
            Opcode::SkipKeyNotPressed(x) => {
                if self.input[self.reg[x] as usize] == 0{
                    self.inc_size = 4;
                }
            },
            Opcode::BcdX(x) => {
                self.mem[self.index_reg] =  self.reg[x] / 100;
                self.mem[self.index_reg + 1] =  self.reg[x] % 100 / 10;
                self.mem[self.index_reg + 2] =  self.reg[x] % 10;
            },
            Opcode::Draw(x, y, z) => {
                self.draw_flag = true;
                let xpos = self.reg[x] as usize;
                let mut hit = 0;
                for row in 0..z as usize{
                    let ypos = self.reg[y] as usize + row;
                    // Read our byte
                    let byte = self.mem[self.index_reg + row];
                    for (i, &b) in [0x80, 0x40, 0x20, 0x10, 0x08, 0x04, 0x02, 0x01].into_iter().enumerate(){
                        if (b&byte) != 0{
                            let dx = match xpos + i{
                                n if n >= 64 => n % 64,
                                n => n
                            };
                            let dy = match ypos{
                                n if n >= 32 => n % 32,
                                n => n
                            };
                            hit = match setPixel(&mut self.canvas, dx, dy, true){
                                true => 1,
                                false => hit
                            }

                            //if self.gfx.get_pixel(dx, dy)[0] == 255{
                            //    self.gfx.set_pixel(dx, dy, &[0,0,0,255]).unwrap();
                            //    hit = 1
                            //}
                            //else{
                                //println!("Draw Pixel at {} {}", dx, dy);
                            //    self.gfx.set_pixel(dx, dy, &[255,255,255,255]).unwrap();
                            //}
                        }
                    }
                }
                self.reg[0xF] = hit;
            },
            _ => {}
        }
        
    }
}

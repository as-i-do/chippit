use crate::opcodes::{Opcode, OpMacro};
use hex::FromHex;

#[derive(Debug, Clone)]
pub enum Token{
    EOF,
    INVALID,
    RETURN,
    CALL,
    JUMP,
    CLEAR,
    RAND,
    LOAD, STORE,
    SPRITE, BCD,
    LOADIMG,
    SETSYMBOL(String),
    SYMBOL(String),

    PLUSEQUAL, //+=
    PLUS, //+
    AND,
    SET, // =
    SUBEQUAL, // -=
    RSUBEQUAL, // =-
    ANDEQUAL, // &=
    OREQUAL, // |=
    XOREQUAL, // ^=
    SHR, SHL,
    EQUAL, // ==
    NEQUAL, // !=
    INDEX,
    DRAW,
    SKIP,
    TIMER, SOUND,
    KEY,
    REGISTER(usize),
    INTEGER(usize),
    COMMENT,
    // Macros
    DRAWAT, //draw@
    RAW,
    BLOCK,
    IMG, FULLIMG, FILENAME(String),
}

impl Token{
    pub fn is_comment(&self) -> bool{
        match self {
            Token::COMMENT => true,
            _ => false,
        }
    }
}
impl From<&str> for Token{
    fn from(src: &str) -> Self{
        match src{
            "+" => Token::PLUS,
            "+=" => Token::PLUSEQUAL,
            "|=" => Token::OREQUAL,
            "^=" => Token::XOREQUAL,
            "&" => Token::AND,
            "&=" => Token::ANDEQUAL,
            "-=" => Token::SUBEQUAL,
            "=-" => Token::RSUBEQUAL,
            "=" => Token::SET,
            "==" => Token::EQUAL,
            "!=" => Token::NEQUAL,
            "i" => Token::INDEX,
            "v0" => Token::REGISTER(0),
            "v1" => Token::REGISTER(1),
            "v2" => Token::REGISTER(2),
            "v3" => Token::REGISTER(3),
            "v4" => Token::REGISTER(4),
            "v5" => Token::REGISTER(5),
            "v6" => Token::REGISTER(6),
            "v7" => Token::REGISTER(7),
            "v8" => Token::REGISTER(8),
            "v9" => Token::REGISTER(9),
            "vA" => Token::REGISTER(10),
            "vB" => Token::REGISTER(11),
            "vC" => Token::REGISTER(12),
            "vD" => Token::REGISTER(13),
            "vE" => Token::REGISTER(14),
            "vF" => Token::REGISTER(15),

            "beep" => Token::SOUND,
            "timer" => Token::TIMER,
            "key" => Token::KEY,
            "jump" => Token::JUMP,
            "call" => Token::CALL,
            "return" => Token::RETURN,
            "clear" => Token::CLEAR,
            "rand" => Token::RAND,

            "<<" => Token::SHL,
            ">>" => Token::SHR,
            "load" => Token::LOAD,
            "store" => Token::STORE,
            "bcd" => Token::BCD,
            "sprite" => Token::SPRITE,
            "draw" => Token::DRAW,
            "skipif" => Token::SKIP,

            //Macros
            "raw" => Token::RAW,
            "draw@" => Token::DRAWAT,
            "block" => Token::BLOCK,
            "img" => Token::IMG,
            "fullscreenimg" => Token::FULLIMG,

            //Dynamic
            n if n.parse::<usize>().is_ok() => Token::INTEGER(n.parse::<usize>().unwrap()),
            n if n.starts_with(":") => Token::SYMBOL(n[1..].to_string()),
            n if n.starts_with(";") => Token::SETSYMBOL(n[1..].to_string()),
            n if n.starts_with("#") => Token::COMMENT,
            n if n.starts_with("FILE=") => Token::FILENAME(n.replace("FILE=", "")),
            n if n.starts_with("x") => {
                // Get from hex..
                let x = Vec::from_hex(n[1..].to_string()).unwrap();
                Token::INTEGER(x.last().cloned().unwrap() as usize)
            }
            _ => Token::INVALID
        }
    }
}

impl From<&[Token]> for Opcode{
    fn from(src: &[Token]) -> Self{
        // Handle the weird opcodes
        if src.len() > 0{
            match src[0]{
                Token::RAW => return Opcode::Macro(OpMacro::RawData(src[1..].iter().map(|x| {
                    match x {
                        Token::INTEGER(y) => (y & 0xFF) as u8,
                        _ => panic!("Got non-integer value in raw")
                    }
                }).collect())),
                _ => ()
            }
        }
        match src{
            //ADDING
            [Token::REGISTER(x), Token::PLUSEQUAL, Token::INTEGER(y)] =>
                Opcode::AddXB(*x, *y as u8),
            [Token::REGISTER(x), Token::PLUSEQUAL, Token::REGISTER(y)] =>
                Opcode::AddXY(*x, *y),
            [Token::INDEX, Token::PLUSEQUAL, Token::REGISTER(x)] =>
                Opcode::IncIfromX(*x),

            //SUBTRACT
            [Token::REGISTER(x), Token::SUBEQUAL, Token::REGISTER(y)] =>
                Opcode::SubXY(*x, *y),
            [Token::REGISTER(x), Token::RSUBEQUAL, Token::REGISTER(y)] =>
                Opcode::SubnXY(*x, *y),

            //BITWISE
            [Token::REGISTER(x), Token::OREQUAL, Token::REGISTER(y)] =>
                Opcode::ORXY(*x, *y),
            [Token::REGISTER(x), Token::ANDEQUAL, Token::REGISTER(y)] =>
                Opcode::ANDXY(*x, *y),
            [Token::REGISTER(x), Token::XOREQUAL, Token::REGISTER(y)] =>
                Opcode::XORXY(*x, *y),
            [Token::REGISTER(x), Token::SHR, Token::REGISTER(y)] =>
                Opcode::RshiftXY(*x, *y),
            [Token::REGISTER(x), Token::SHL, Token::REGISTER(y)] =>
                Opcode::LshiftXY(*x, *y),

            //SETTERS
            [Token::REGISTER(x), Token::SET, Token::INTEGER(y)] =>
                Opcode::SetXfromB(*x, *y as u8),
            [Token::REGISTER(x), Token::SET, Token::REGISTER(y)] =>
                Opcode::SetXfromY(*x, *y),
            [Token::INDEX, Token::SET, Token::INTEGER(n)] =>
                Opcode::SetIfromN(*n as usize),
            [Token::SOUND, Token::SET, Token::REGISTER(x)] =>
                Opcode::SetSTfromX(*x),
            [Token::TIMER, Token::SET, Token::REGISTER(x)] =>
                Opcode::SetDTfromX(*x),
            [Token::REGISTER(x), Token::SET, Token::TIMER] =>
                Opcode::SetXfromDT(*x),

            //MOVING
            [Token::CLEAR] =>
                Opcode::ClearScreen,
            [Token::JUMP, Token::INTEGER(n)] =>
                Opcode::Jump(*n as usize),
            [Token::JUMP, Token::INTEGER(n), Token::PLUS, Token::REGISTER(0)] =>
                Opcode::JumpPlusV0(*n as usize),
            [Token::CALL, Token::INTEGER(n)] =>
                Opcode::Call(*n as usize),
            [Token::RETURN] =>
                Opcode::Return,

            //SKIPS AND KEYS
            [Token::SKIP, Token::REGISTER(x), Token::EQUAL, Token::INTEGER(b)] =>
                Opcode::SkipXisB(*x, *b as u8),
            [Token::SKIP, Token::REGISTER(x), Token::NEQUAL, Token::INTEGER(b)] =>
                Opcode::SkipXnotB(*x, *b as u8),
            [Token::SKIP, Token::REGISTER(x), Token::EQUAL, Token::REGISTER(y)] =>
                Opcode::SkipXisY(*x, *y),
            [Token::SKIP, Token::REGISTER(x), Token::NEQUAL, Token::REGISTER(y)] =>
                Opcode::SkipXnotY(*x, *y),
            [Token::SKIP, Token::KEY, Token::EQUAL, Token::REGISTER(x)] =>
                Opcode::SkipKeyPressed(*x),
            [Token::SKIP, Token::KEY, Token::NEQUAL, Token::REGISTER(x)] =>
                Opcode::SkipKeyNotPressed(*x),
            [Token::KEY, Token::EQUAL, Token::REGISTER(x)] =>
                Opcode::WaitKey(*x),

            //DRAWING
            [Token::DRAW, Token::REGISTER(x), Token::REGISTER(y), Token::INTEGER(n)] =>
                Opcode::Draw(*x, *y, *n as u8),
            [Token::SPRITE, Token::REGISTER(x)] =>
                Opcode::Digit(*x),
            [Token::DRAWAT, Token::INTEGER(i), Token::REGISTER(x), Token::REGISTER(y), Token::INTEGER(n)] =>
                Opcode::Macro(OpMacro::DrawAt(*i as usize, *x, *y, *n as u8)),
           
            //MEMORY
            [Token::LOAD, Token::REGISTER(x)] =>
                Opcode::ReadMem(*x),
            [Token::STORE, Token::REGISTER(x)] =>
                Opcode::WriteMem(*x),
            [Token::BCD, Token::REGISTER(x)] =>
                Opcode::BcdX(*x),

            //RANDOM
            [Token::RAND, Token::REGISTER(x), Token::AND, Token::INTEGER(y)] =>
                Opcode::RandXB(*x, *y as u8),
            [Token::RAND, Token::REGISTER(x)] =>
                Opcode::RandXB(*x, 255),

            [Token::BLOCK, Token::INTEGER(n), Token::INTEGER(x)] =>
                Opcode::Macro(OpMacro::Block(*n, *x as u8)),
            [Token::FULLIMG, Token::FILENAME(s), Token::INTEGER(selfref)] => {
                let im = image::open(s).expect("Failed to open image");
                let bim = im.to_bgr();
                let im_width = bim.width();
                let im_height = bim.height();
                let im = im.grayscale().raw_pixels();
                if (im_width != 64) | (im_height != 32){
                    panic!("INVALID DIMENSIONS == MUST ME 32x64");
                };
                let mut output: Vec<u8> = vec!();
                // Break the image into 8x8 chunks for simplicity
                for block in im.chunks(512){
                   for x in 0..8{
                       for y in 0..8{
                           let start = (64 * y) + (8*x);
                           let bytes: Vec<u8> = block[start..start+8].iter().enumerate().map(|(i, &v)|{
                               let mut b: u8 = (v > 10) as u8;
                               b <<= 7 - i;
                               return b
                           }).collect();
                           output.push(
                               bytes[0] | bytes[1] | bytes[2] | bytes[3] |
                               bytes[4] | bytes[5] | bytes[6] | bytes[7]
                           );
                       }
                   } 
                };
                // With our output constructed, we run the macro
                Opcode::Macro(OpMacro::FullScreenImage(output, *selfref))
            },
            [Token::IMG, Token::FILENAME(s),
            Token::INTEGER(x), Token::INTEGER(y), Token::INTEGER(w), Token::INTEGER(h)] => {
                let mut output = vec!();
                let im = image::open(s).expect("Failed to open image");
                let bim = im.to_bgr();
                let im_width = bim.width();
                let im_height = bim.height();
                let im = im.grayscale().raw_pixels();
                if *w > 8{
                    panic!("Width must be smaller than 8");
                }
                if *h > 15{
                    panic!("Height mit be smaller than 15");
                }
                if *w + *x > im_width as usize{
                    panic!("Tried to extract out of bounds (width)")
                }
                if *h + *y > im_height as usize{
                    panic!("Tried to extract out of bounds (height)")
                }
                for ih in 0..*h{
                    let start = ((ih + *y) * im_width as usize) + *x;
                    let mut bytes: Vec<u8> = im[start..start+*w].iter().enumerate().map(|(i, &v)|{
                        let mut b: u8 = (v > 10) as u8;
                        b <<= 7 - i;
                        return b
                    }).collect();
                    for _ in 0..(8-*w){
                        bytes.push(0);
                    }
                    output.push(
                        bytes[0] | bytes[1] | bytes[2] | bytes[3] |
                        bytes[4] | bytes[5] | bytes[6] | bytes[7]
                    );
                }
                if output.len() % 2 != 0 {
                    output.push(0);
                }
                println!("{:x?}", output);
                Opcode::Macro(OpMacro::RawData(output))
            },
            v => panic!("What is this shit? Failed at {:?}", v)
        }
    }
}

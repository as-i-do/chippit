use crate::opcodes::Opcode;
use crate::tokens::Token;
use std::{num::ParseIntError};
use std::collections::HashMap;

pub fn decode_hex(s: &str) -> Result<Vec<u8>, ParseIntError> {
    (0..s.len())
        .step_by(2)
        .map(|i| u8::from_str_radix(&s[i..i + 2], 16))
        .collect()
}

pub struct Compiler{
    thiccness: Vec<usize>,
    tokens: Vec<Vec<Token>>,
    pos: usize,
}


impl Compiler{
    pub fn new(data:Vec<String>) -> Compiler{
        // First pass, tokenize, calculate thiccness, get symbols
        let mut tokens: Vec<Vec<Token>> = vec!();
        let mut thiccness: Vec<usize> = vec!();
        let mut symbols: HashMap<String, usize> = HashMap::new();
        // Base memory size for chip8 interpreter
        let base_size = 512;
        for (i, line) in data.iter().enumerate(){
            // Tokenize
            if line.split_whitespace().next().unwrap().starts_with('#'){
                tokens.push(vec!(Token::COMMENT));
            }
            else{
                tokens.push(line.split_whitespace().map(|x| {
                    let x: Token = x.into();
                    return x
                }).collect());
            }

            // Collect symbols...
            let mut is_symbol = false;
            match &tokens[i][0] {
                Token::SETSYMBOL(s) => {
                    // Make a new string lol rust
                    let s = String::from(s);
                    symbols.insert(s, thiccness[0..i].iter().sum());
                    is_symbol = true;
                },
                _ => ()
            };
            // Delete the symbol from the tokens...
            if is_symbol {
                tokens[i] = tokens[i][1..].to_vec();
            }

            // Compute byte thiccness
            if tokens[i].len() == 0 || tokens[i][0].is_comment(){
                thiccness.push(0)
            }
            else {
                // Pre-compute the token, subbing INTEGER(0) for any symbols...
                // This is a big hack and limits us to numeric symbols
                let shadow_vec: Vec<Token> = (0..tokens[i].len()).map(|x|{
                    match &tokens[i][x]{
                        Token::SYMBOL(_) => Token::INTEGER(0),
                        s => s.clone()
                    }
                }).collect();
                let shadow: Opcode = shadow_vec.as_slice().into();
                let thic: usize = match shadow{
                    Opcode::Macro(x) => x.thiccness(),
                    _ => 2
                };
                thiccness.push(thic);
            }
        }

        // Second pass -- replace all symbol referencs
        // Gotta do ugly forloops here
        for line_index in 0..tokens.len(){
            for token_index in 0..tokens[line_index].len(){
                match &tokens[line_index][token_index]{
                    Token::SYMBOL(s) => {
                        println!("invoking {}", s);
                        tokens[line_index][token_index] =
                            Token::INTEGER(*symbols.get(s).unwrap() + base_size);
                    },
                    _ => ()
                }
            }
        }

        // Return
        Compiler{
            tokens,
            thiccness,
            pos: 0,
        }
    }
}

impl Iterator for Compiler{
    type Item = Opcode;

    fn next(&mut self) -> Option<Self::Item>{
        if self.pos >= self.tokens.len(){
            return None
        }
        // Skip blank / comment lines...
        while self.thiccness[self.pos] == 0{
            self.pos += 1;
        }
        self.pos += 1;
        Some(self.tokens[self.pos-1].as_slice().into())
    }
}


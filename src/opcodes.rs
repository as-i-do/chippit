fn snack16(a:u8, b:u8, c:u8) -> u16 {
    // UTILITY Converts 3 nibbles to a u16
    (((a as u16)<<8) | ((b as u16)<<4) | c as u16)
}

fn snack8(a: u8, b:u8) -> u8 {
    // UTILITY converts 2 nibbles to a u8
    (a<<4) | b
} 


#[derive(Debug, Clone)]
pub enum Opcode{
    // Full set of CHIP8 Opcodes
    Macro(OpMacro),
    SYS(u16),
    ClearScreen,
    Return,
    Jump(usize),
    JumpPlusV0(usize),
    Call(usize),
    SkipXisB(usize, u8),
    SkipXnotB(usize, u8),
    SkipXisY(usize, usize),
    SkipXnotY(usize, usize),
    SetXfromB(usize, u8),
    SetXfromY(usize, usize),
    SetIfromN(usize),
    SetSTfromX(usize),
    SetDTfromX(usize),
    SetXfromDT(usize),
    AddXB(usize, u8),
    AddXY(usize, usize),
    IncIfromX(usize),
    SubXY(usize, usize),
    SubnXY(usize, usize),
    ORXY(usize, usize),
    ANDXY(usize, usize),
    XORXY(usize, usize),
    LshiftXY(usize, usize),
    RshiftXY(usize, usize),
    WaitKey(usize),
    SkipKeyNotPressed(usize),
    SkipKeyPressed(usize),
    RandXB(usize, u8),
    Draw(usize, usize, u8),
    Digit(usize),
    BcdX(usize),
    WriteMem(usize),
    ReadMem(usize),
}


#[derive(Debug, Clone)]
pub enum OpMacro{
// Special macros from the interpreter. Non-functioning as Opcodes
// These are here for compiling / interpreting, and not part of the
// emulator instruction set.
    // Does nothing
    Nothing,
    // List of raw bytes
    RawData(Vec<u8>),
    //A full screen image. Consumes 582 bytes
    FullScreenImage(Vec<u8>, usize),
    // Does an i= followed by a draw
    DrawAt(usize, usize, usize, u8),
    // Fills N bytes with a u8,
    Block(usize, u8),
}

impl OpMacro{
    pub fn thiccness(&self) -> usize{
        match self{
            OpMacro::RawData(v) => v.len(),
            OpMacro::DrawAt(..) => 4,
            OpMacro::FullScreenImage(..) => 582,
            OpMacro::Block(u, _) => *u,
            _ => 2,
        }
    }
    pub fn expand(&self) -> Vec<u8>{
        match self{
            OpMacro::RawData(a) => a.clone(),
            OpMacro::Block(n, x) => {
                let mut m = vec!();
                for _ in 0..*n{
                    m.push(*x);
                };
                return m.clone()

            },
            OpMacro::DrawAt(i, x, y, n) => {
                let mut mov: Vec<u8> = Opcode::SetIfromN(*i).into();
                let drw: Vec<u8> = Opcode::Draw(*x, *y, *n).into();
                mov.extend(&drw);
                mov
            }
            OpMacro::FullScreenImage(v, selfref) => {
                let stash_address = selfref + (27 * 2);
                let img_address = &stash_address + 16;
                let row_fn = selfref + (12 * 2);
                let block_fn = &row_fn + (11 * 2);
                let commands = vec!(
                    //Stash Mem
                    Opcode::SetIfromN(stash_address),
                    Opcode::WriteMem(16),
                    //Set up vars
                    Opcode::SetXfromB(2, 0), //y
                    Opcode::SetXfromB(3, 8), //increment
                    Opcode::SetIfromN(img_address),
                    Opcode::Call(row_fn),
                    Opcode::Call(row_fn),
                    Opcode::Call(row_fn),
                    Opcode::Call(row_fn),
                    Opcode::SetIfromN(stash_address),
                    Opcode::ReadMem(16),
                    Opcode::Return,
                    //Row Function
                    Opcode::SetXfromB(1, 0), //reset x
                    Opcode::Call(block_fn),
                    Opcode::Call(block_fn),
                    Opcode::Call(block_fn),
                    Opcode::Call(block_fn),
                    Opcode::Call(block_fn),
                    Opcode::Call(block_fn),
                    Opcode::Call(block_fn),
                    Opcode::Call(block_fn),
                    Opcode::AddXY(2, 3),
                    Opcode::Return,
                    //Block Function
                    Opcode::Draw(1, 2, 8),
                    Opcode::AddXY(1, 3),
                    Opcode::IncIfromX(3),
                    Opcode::Return,
                );
                let mut data = vec!();
                // Convert our list of commands into binary
                for x in commands.into_iter(){
                    let p: Vec<u8> = x.into();
                    for y in p.into_iter(){
                        data.push(y);
                    };
                };
                // This is the block for storing the current registers
                data.extend(&[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
                data.extend(v.as_slice());
                return data
                
            },
            OpMacro::Nothing => vec!(0, 0)
        }
    }
}
fn nibble(int:usize) -> usize{
    let x = int & 15;
    x
}

fn midnibs(x:usize, y:usize) -> u16{
    // Puts nibbles X Y into 0x0XY0
    ((nibble(x) << 8) | (nibble(y) << 4)) as u16
}

fn nibbyte(x:usize, y:u8) -> u16{
    // Puts nibbles X Y into 0x0XYY
    ((nibble(x) << 8) | y as usize) as u16
}

fn snib(x:usize) -> u16{
    // Puts a single nibble into 0x0X00
    (x << 8) as u16
}


impl From<Opcode> for Vec<u8>{
    fn from(src: Opcode) -> Self{
        let big = match src{
            Opcode::SYS(n) =>
                0x0000 | (0x0FFF & n) as u16,
            Opcode::ClearScreen =>
                0x00E0,
            Opcode::Return =>
                0x00EE,
            Opcode::Jump(x) =>
                0x1000 | (0x0FFF & x) as u16,
            Opcode::JumpPlusV0(x) =>
                0xB000 | (0x0FFF & x) as u16,
            Opcode::Call(x) =>
                0x2000 | (0x0FFF & x) as u16,
            Opcode::SkipXisB(x, b) =>
                0x3000 | nibbyte(x, b),
            Opcode::SkipXnotB(x, b) =>
                0x4000 | nibbyte(x, b),
            Opcode::SkipXisY(x, y) =>
                0x5000 | midnibs(x, y),
            Opcode::SkipXnotY(x, y) =>
                0x9000 | midnibs(x, y),
            Opcode::SetXfromB(x, b) =>
                0x6000 | nibbyte(x, b),
            Opcode::SetXfromY(x, y) =>
                0x8000 | midnibs(x, y),
            Opcode::SetIfromN(n) =>
                0xA000 | (0x0FFF & n) as u16,
            Opcode::SetSTfromX(x) =>
                0xF018 | snib(x),
            Opcode::SetDTfromX(x) =>
                0xF015 | snib(x),
            Opcode::SetXfromDT(x) =>
                0xF007 | snib(x),
            Opcode::AddXB(x, b) =>
                0x7000 | nibbyte(x, b),
            Opcode::AddXY(x, y) =>
                0x8004 | midnibs(x, y),
            Opcode::IncIfromX(x) => 
                0xF01E | snib(x),
            Opcode::SubXY(x, y) =>
                0x8005 | midnibs(x, y),
            Opcode::SubnXY(x, y) =>
                0x8007 | midnibs(x, y),
            Opcode::ORXY(x, y) =>
                0x8001 | midnibs(x, y),
            Opcode::ANDXY(x, y) =>
                0x8002 | midnibs(x, y),
            Opcode::XORXY(x, y) =>
                0x8003 | midnibs(x, y),
            Opcode::LshiftXY(x, y) =>
                0x800E | midnibs(x, y),
            Opcode::RshiftXY(x, y) =>
                0x8006 | midnibs(x, y),
            Opcode::WaitKey(x) =>
                0xF00A | snib(x),
            Opcode::SkipKeyNotPressed(x) =>
                0xE0A1 | snib(x),
            Opcode::SkipKeyPressed(x) =>
                0xE09E | snib(x),
            Opcode::RandXB(x, b) =>
                0xC000 | nibbyte(x, b),
            Opcode::Draw(x, y, b) =>
                0xD000 | midnibs(x, y) | (0x0FF & b as u16),
            Opcode::Digit(x) =>
                0xF029 | snib(x),
            Opcode::BcdX(x) =>
                0xF033 | snib(x),
            Opcode::WriteMem(x) =>
                0xF055 | snib(x),
            Opcode::ReadMem(x) =>
                0xF065 | snib(x),
            Opcode::Macro(m) => return m.expand(),
        };
        let b1 = ((big & 0xFF00) >> 8) as u8;
        let b2 = (big & 0x00FF) as u8;
        vec!(b1, b2)
    }
}

impl From<(u8, u8)> for Opcode{
    fn from(src: (u8, u8)) -> Self{
        let (x, y) = src;
        let a = (0xF0 & x) >> 4;
        let b = 0x0F & x;
        let c = (0xF0 & y) >> 4;
        let d = 0x0F & y;
        match (a, b, c, d){
            (0, 0, 0xE, 0) => Opcode::ClearScreen,
            (0, 0, 0xE, 0xE) => Opcode::Return,
            (0, a, b, c) => Opcode::SYS(snack16(a, b, c)),
            (1, a, b, c) => Opcode::Jump(snack16(a, b, c) as usize),
            (2, a, b, c) => Opcode::Call(snack16(a, b, c) as usize),
            (3, x, a, b) => Opcode::SkipXisB(x as usize, snack8(a, b)),
            (4, x, a, b) => Opcode::SkipXnotB(x as usize, snack8(a, b)),
            (5, x, y, 0) => Opcode::SkipXisY(x as usize, y as usize),
            (6, x, a, b) => Opcode::SetXfromB(x as usize, snack8(a, b)),
            (7, x, a, b) => Opcode::AddXB(x as usize, snack8(a, b)),
            (8, x, y, 0) => Opcode::SetXfromY(x as usize, y as usize),
            (8, x, y, 1) => Opcode::ORXY(x as usize, y as usize),
            (8, x, y, 2) => Opcode::ANDXY(x as usize, y as usize),
            (8, x, y, 3) => Opcode::XORXY(x as usize, y as usize),
            (8, x, y, 4) => Opcode::AddXY(x as usize, y as usize),
            (8, x, y, 5) => Opcode::SubXY(x as usize, y as usize),
            (8, x, y, 6) => Opcode::RshiftXY(x as usize, y as usize),
            (8, x, y, 7) => Opcode::SubnXY(x as usize, y as usize),
            (8, x, y, 0xE) => Opcode::LshiftXY(x as usize, y as usize),
            (9, x, y, 0) => Opcode::SkipXnotY(x as usize, y as usize),
            (0xA, a, b, c) => Opcode::SetIfromN(snack16(a, b, c) as usize),
            (0xB, a, b, c) => Opcode::JumpPlusV0(snack16(a, b, c) as usize),
            (0xC, x, a, b) => Opcode::RandXB(x as usize, snack8(a, b)),
            (0xD, x, y, z) => Opcode::Draw(x as usize, y as usize, z),
            (0xE, x, 9, 0xE) => Opcode::SkipKeyPressed(x as usize),
            (0xE, x, 0xA, 1) => Opcode::SkipKeyNotPressed(x as usize),
            (0xF, x, 0, 7) => Opcode::SetXfromDT(x as usize),
            (0xF, x, 0, 0xA) => Opcode::WaitKey(x as usize),
            (0xF, x, 1, 5) => Opcode::SetDTfromX(x as usize),
            (0xF, x, 1, 8) => Opcode::SetSTfromX(x as usize),
            (0xF, x, 1, 0xE) => Opcode::IncIfromX(x as usize),
            (0xF, x, 2, 9) => Opcode::Digit(x as usize),
            (0xF, x, 3, 3) => Opcode::BcdX(x as usize),
            (0xF, x, 5, 5) => Opcode::WriteMem(x as usize),
            (0xF, x, 6, 5) => Opcode::ReadMem(x as usize),
            (_,_,_,_) => Opcode::Macro(OpMacro::Nothing),
        }
    }
}

use std::env;
use chippit::compiler::Compiler;
use std::io::{self, BufReader, Write};
use std::io::prelude::*;
use std::fs::File;

fn main() -> io::Result<()> {
    let args: Vec<String> = env::args().collect();
    let filename = match &args.len() {
        1 => "./test.src",
        _ => &args[1]
    };
    let f = File::open(filename)?;
    let f = BufReader::new(f);
    let data = f.lines().map(|x| x.unwrap()).collect();
    let inter = Compiler::new(data);
    let mut out = File::create("out.ch8")?;
    for x in inter{
        println!("{:?}", x);
        let y: Vec<u8> = x.into();
        println!("{:x?}", y);
        out.write_all(y.as_slice())?;
    }

    Ok(())
}

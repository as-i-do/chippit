use std::env;
use std::fs::File;
use std::io::{Read, BufReader};
use std::io::prelude::*;
use chippit::emulator::Emulator;
use chippit::compiler::Compiler;
use flatland::eventhandlers::{Event, Key};


fn recompile(e: &mut Emulator){
    let args: Vec<String> = env::args().collect();
    let filename = match &args.len() {
        1 => "./test.src",
        _ => &args[1]
    };
    let f = File::open(filename).unwrap();
    let f = BufReader::new(f);
    let cmp = Compiler::new(f.lines().map(|x| x.unwrap()).collect());
    let mut stream: Vec<u8> = vec!();
    for x in cmp{
        println!("{:?}", x);
        let mut y: Vec<u8> = x.into();
        stream.append(&mut y);
    }
    e.update_program(stream.as_slice());
}


fn main(){
    let mut emu = Emulator::new(
        None,
        vec!(
            (Event::KeyPress(Key::T), recompile),
        ));
    recompile(&mut emu);
    emu.run();
}

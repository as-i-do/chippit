// Re-usable implementation of an emulator with a manual and hot reload option
use std::env;
use std::{thread, time};
use crate::core::Chippit;
use flatland::screens::{Renderer, window::Screen};
use flatland::eventhandlers::{EventHandler, Event, Key};

pub type EmulatorCallback = fn(e: &mut Emulator);

pub struct Emulator {
    keymap: [Key;16],
    frame_limiter: time::Instant,
    input: [bool;16],
    kill: bool,
    custom_callbacks: Vec<EmulatorCallback>,
    custom_events: Vec<Event>,
    pub chip: Chippit,
}

impl Emulator {
    pub fn new(keys: Option<[Key;16]>, custom_commands: Vec<(Event, EmulatorCallback)>) -> Emulator {
        let keymap: [Key;16] = match keys {
            Some(control_array) => control_array,
            None => [
                Key::Q,
                Key::W,
                Key::E,
                Key::R,
                Key::A,
                Key::S,
                Key::D,
                Key::F,
                Key::Z,
                Key::X,
                Key::C,
                Key::V,
                Key::H,
                Key::J,
                Key::K,
                Key::L,
            ]
        };
        let mut custom_events = vec!();
        let mut custom_callbacks = vec!();
        for x in custom_commands{
            let (event, callback) = x;
            custom_events.push(event);
            custom_callbacks.push(callback);
        };
        let mut chip = Chippit::load(&[]);
        Emulator{
            frame_limiter: time::Instant::now(),
            keymap,
            kill: false,
            input: [false; 16],
            custom_callbacks,
            custom_events,
            chip
        } 
    }

    pub fn update_program(&mut self, program: &[u8]){
        self.chip = Chippit::load(program);
    }

    pub fn run(&mut self){
        let (mut screen, mut events) = Screen::create(64, 32).unwrap();
        screen.refresh();
        let mut running = true;

        while running {
            match self.chip.cycle(&self.input){
                Some(new_screen) => {
                    while time::Instant::now().duration_since(self.frame_limiter).subsec_millis() < 20{
                    };
                    screen.draw(&new_screen[..]).unwrap();
                    self.frame_limiter = time::Instant::now()
                },
                None => ()
            };
            // Handle Keys
            events.catch(|event| {
                // Catch our custom events first
                let custom_e = self.custom_events.iter().position(|&s| s == event);
                match custom_e {
                    Some(x) => (self.custom_callbacks[x])(self),
                    None => ()
                }

                match event{
                    Event::Close => running = false,
                    Event::Resize(x,y) => {
                        screen.resize(x, y).unwrap();
                        screen.refresh();
                    },
                    Event::KeyPress(key) => {
                        let key_index = self.keymap.iter().position(|&k| k == key);
                        match key_index {
                            Some(x) => self.input[x] = true,
                            None => ()
                        };
                    },
                    Event::KeyRelease(key) => {
                        let key_index = self.keymap.iter().position(|&k| k == key);
                        match key_index {
                            Some(x) => self.input[x] = false,
                            None => ()
                        };
                    },
                    _ => ()
                }
            });

            if self.kill{
                running = false
            }
        }
    }
}


